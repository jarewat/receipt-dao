/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author jarew
 */
public class OrderCustomer {

    private int id;
    private Date created;
    private User seller;
    private Customer customer;
    private ArrayList<OrderCustomerDetail> orderDetail;

    public OrderCustomer(int id, Date created, User seller, Customer customer) {
        this.id = id;
        this.seller = seller;
        this.customer = customer;
        this.orderDetail = new ArrayList<>();
    }

    public OrderCustomer(User seller, Customer customer) {
        this(-1, null, seller, customer);
    }

    public void addOrderDetail(int id, Product product, int amount, double price) {
        for (int i = 0; i < orderDetail.size(); i++) {
            OrderCustomerDetail r = orderDetail.get(i);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
                return;
            }
        }
        orderDetail.add(new OrderCustomerDetail(id, product, amount, price, this));
    }

    public void addOrderDetail(int id, Product product, int amount) {
        addOrderDetail(id, product, amount, product.getPrice());
    }

    public void addOrderDetail(Product product, int amount, double price) {
        addOrderDetail(-1, product, amount, price);
    }

    public void addOrderDetail(Product product, int amount) {
        addOrderDetail(-1, product, amount, product.getPrice());
    }

    public void deleteOrderDetail(int row) {
        orderDetail.remove(row);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ArrayList<OrderCustomerDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(ArrayList<OrderCustomerDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    @Override
    public String toString() {
        String str = "OrderCustomer{" + "id=" + id + ", created=" + created + ", seller=" + seller + ", customer=" + customer + ", total=" + this.getTotal() + "}\n";
        for (OrderCustomerDetail or : orderDetail) {
            str += or.toString() + "\n";
        }
        return str;
    }

    public double getTotal() {
        double total = 0;
        for (OrderCustomerDetail orderCustomerDetail : orderDetail) {
            total += orderCustomerDetail.getTotal();
        }
        return total;
    }

}
