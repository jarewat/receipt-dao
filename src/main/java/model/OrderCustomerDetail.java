/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author jarew
 */
public class OrderCustomerDetail {

    private int id;
    private Product product;
    private int amount;
    private double price;
    private OrderCustomer orderCustomer;

    public OrderCustomerDetail(int id, Product product, int amount, double price, OrderCustomer orderCustomer) {
        this.id = id;
        this.product = product;
        this.amount = amount;
        this.price = price;
        this.orderCustomer = orderCustomer;
    }

    public OrderCustomerDetail(Product product, int amount, double price, OrderCustomer orderCustomer) {
        this(-1, product, amount, price, orderCustomer);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public OrderCustomer getOrderCustomer() {
        return orderCustomer;
    }

    public void setOrderCustomer(OrderCustomer orderCustomer) {
        this.orderCustomer = orderCustomer;
    }
    
    public void addAmount(int amount){
        this.amount += amount;
    }

    @Override
    public String toString() {
        return "OrderCustomerDetail{" + "id=" + id + ", product=" + product + ", amount=" + amount + ", price=" + price + ", total=" + this.getTotal() + "}";
    }

    public double getTotal() {
        return amount * price;
    }
}
