/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprojectv1.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.OrderCustomer;
import model.OrderCustomerDetail;
import model.Product;
import model.User;

/**
 *
 * @author jarew
 */
public class OrderCustomerOao implements DaoInterface<OrderCustomer> {

    @Override
    public int add(OrderCustomer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO order_customer (  or_cus_total, user_id, customer_id ) VALUES ( ?, ?, ?); ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setDouble(1, object.getTotal());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setInt(3, object.getCustomer().getId());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (OrderCustomerDetail order : object.getOrderDetail()) {
                String sqlDetail = "INSERT INTO order_customer_item (\n"
                        + "                                    product_id,\n"
                        + "                                    or_cus_id,\n"
                        + "                                    or_cus_item_price,\n"
                        + "                                    or_cus_item_amount\n"
                        + "                                )\n"
                        + "                                VALUES (?, ?, ?,?); ";
                PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, order.getProduct().getId());
                stmtDetail.setInt(2, order.getOrderCustomer().getId());
                stmtDetail.setDouble(3, order.getPrice());
                stmtDetail.setInt(4, order.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = result.getInt(1);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return id;
    }

    @Override
    public ArrayList<OrderCustomer> getAll() {
        ArrayList<OrderCustomer> list = new ArrayList<>();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT or_cus_id,\n"
                    + "       or_cus_date,\n"
                    + "       or_cus_total,\n"
                    + "       u.user_id as user_id,\n"
                    + "       u.user_name as user_name,\n"
                    + "       u.user_tel as user_tel,\n"
                    + "       c.customer_id as customer_id,\n"
                    + "       c.customer_name as customer_name,\n"
                    + "       c.customer_tel as customer_tel\n"
                    + "       \n"
                    + " FROM order_customer orcus, customer c, user u\n"
                    + " WHERE orcus.customer_id = c.customer_id AND orcus.user_id = u.user_id\n"
                    + " ORDER BY or_cus_date DESC;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("or_cus_id");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_cus_date"));
                double total = result.getDouble("or_cus_total");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                OrderCustomer order = new OrderCustomer(id, date, new User(userId, userName, userTel), new Customer(customerId, customerName, customerTel));
                list.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(OrderCustomerOao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return list;
    }

    @Override
    public OrderCustomer getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT or_cus_id,\n"
                    + "       or_cus_date,\n"
                    + "       or_cus_total,\n"
                    + "       u.user_id as user_id,\n"
                    + "       u.user_name as user_name,\n"
                    + "       u.user_tel as user_tel,\n"
                    + "       c.customer_id as customer_id,\n"
                    + "       c.customer_name as customer_name,\n"
                    + "       c.customer_tel as customer_tel\n"
                    + "       \n"
                    + " FROM order_customer orcus, customer c, user u\n"
                    + " WHERE orcus.or_cus_id = ? AND orcus.customer_id = c.customer_id AND orcus.user_id = u.user_id;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                int orid = result.getInt("or_cus_id");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("or_cus_date"));
                double total = result.getDouble("or_cus_total");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                OrderCustomer order = new OrderCustomer(orid, date, new User(userId, userName, userTel), new Customer(customerId, customerName, customerTel));
                //Order Customer Detali
                getOrderDetail(con, id, order);

                return order;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(OrderCustomerOao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return null;
    }

    private void getOrderDetail(Connection con, int id, OrderCustomer order) throws SQLException {
        String sqlDetail = "SELECT or_cus_item_id,\n"
                + "       p.product_id as product_id,\n"
                + "       p.product_name as product_name,\n"
                + "       or_cus_id,\n"
                + "       or_cus_item_price,\n"
                + "       or_cus_item_amount\n"
                + "  FROM order_customer_item oci, product p\n"
                + "  WHERE or_cus_id = ? AND oci.product_id= p.product_id;";
        PreparedStatement stmtDetail = con.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();

        while (resultDetail.next()) {
            int orderDetailId = resultDetail.getInt("or_cus_item_id");
            int productId = resultDetail.getInt("or_cus_item_id");
            String productName = resultDetail.getString("product_name");
            double pric = resultDetail.getDouble("or_cus_item_price");
            int amount = resultDetail.getInt("or_cus_item_amount");
            Product product = new Product(productId, productName, pric);
            order.addOrderDetail(orderDetailId, product, amount);
        }
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM order_customer\n"
                    + "      WHERE or_cus_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }

    @Override
    public int update(OrderCustomer object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Latte", 45);
        Product p2 = new Product(2, "Espresso", 50);
        User seller = new User(1, "Nat", "0821234581");
        Customer customer = new Customer(1, "Jarewat", "0809999999");
        OrderCustomer order = new OrderCustomer(seller, customer);
        order.addOrderDetail(p1, 1, p1.getPrice());
        order.addOrderDetail(p2, 2, p2.getPrice());
        OrderCustomerOao dao = new OrderCustomerOao();
        dao.add(order);
        System.out.println(dao.getAll());
        System.out.println(dao.getById(order.getId()));
        dao.delete(23);
    }

}
