/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprojectv1.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.User;

/**
 *
 * @author jarew
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer ( customer_name, customer_phone ) VALUES ( ?, ? );";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList<Customer> list = new ArrayList<>();
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT customer_id, customer_name, customer_phone, customer_point FROM customer;";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("customer_id");
                String name = result.getString("customer_name");
                String phone = result.getString("customer_phone");
                int point = result.getInt("customer_point");
                Customer customer = new Customer(id, name, phone, point);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return list;
    }

    @Override
    public Customer getById(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();

        try {
            String sql = "SELECT customer_id, customer_name, customer_phone, customer_point FROM customer WHERE customer_id =" + id;
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cid = result.getInt("customer_id");
                String name = result.getString("customer_name");
                String phone = result.getString("customer_phone");
                int point = result.getInt("customer_point");
                Customer customer = new Customer(cid, name, phone, point);
                Database.close();
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE customer_id = ?;";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET customer_name = ?, customer_phone = ?, customer_point = ? WHERE customer_id =?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setInt(3, object.getPoint());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return row;
    }

    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.getById(1));
        int id = dao.add(new Customer(-1, "Jaruwat", "0821548612",0));
        System.out.println("id : " + id);
        Customer lastCustomer = dao.getById(id);
        System.out.println("last" + lastCustomer);
        lastCustomer.setPoint(30);
        dao.update(lastCustomer);
        Customer updateCustomer = dao.getById(id);
        System.out.println("update" + updateCustomer);
        dao.delete(id);
    }
}
