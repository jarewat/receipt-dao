/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprojectv1.poc;

import model.Customer;
import model.OrderCustomer;
import model.Product;
import model.User;

/**
 *
 * @author jarew
 */
public class TestOrderCustomer {

    public static void main(String[] args) {
        Product p1 = new Product(1, "Milk", 30);
        Product p2 = new Product(2, "Latte", 40);
        User seller = new User("Nat", "084123546", "");
        Customer customer = new Customer("Frist", "086421354");
        OrderCustomer order = new OrderCustomer(seller, customer);
        order.addOrderDetail(p1, 1, p1.getPrice());
        order.addOrderDetail(p2, 2, p2.getPrice());
        System.out.println(order);
        order.deleteOrderDetail(0);
        System.out.println(order);
        order.addOrderDetail(p1, 1, p1.getPrice());
        order.addOrderDetail(p2, 2, p2.getPrice());
        order.addOrderDetail(p1, 1, p1.getPrice());
        System.out.println(order);

    }
}
